<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VMessage</title>

	<style type="text/css">
				.center-block {
            display: block;
            margin-left: auto;
            margin-right: auto;
            max-width: 7000px;
          }
        #dialog{
            padding: 20px;
        }
        .mess{
            background: #bcedbe;
            border-bottom: 1px solid #73b382;
            padding: 20px;
        }
        .navbar-inverse{
            background-color: #229b6a !important;
            border-color: darkgray !important;
        }
        .active>a{
            background-color: darkgray !important;
        }
        .navbar-brand{
                color: #c6d6c4 !important;
        }
                .navbar-inverse .navbar-nav>li>a{
                    color: #fff !important;
                }
        a{
            color: #2d6125 !important;
        }

    </style>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
  <div class="navbar navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">VMessage</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li <?php if($active_link=='messages')echo 'class="active"';?>><a href="messages.php">Messages</a></li>
            <li <?php if($active_link=='send_mess')echo 'class="active"';?>><a href="send_mess.php">Write new</a></li>
            <li><a href="exit.php">Log out</a></li>
          </ul>
        </div>
      </div>
    </div>